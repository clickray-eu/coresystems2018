function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).css({ "display": "none" })
        }
    })
}

function showAsterix() {
    $('form label span:first-of-type').each(function() {
        if ($(this).text() != "") {
            $(this).siblings('.hs-form-required').css({ "display": "inline-block" });
        }
    })
}

// Select


function selectSearch() {


        var searchContent = '';

        $('.dropdown-list .search').keyup(function(e) {

            var searchedWord = e.target.value.toLowerCase();
            var searchedGroup = $(this).parent().find('li').not('.search');


    
           


            searchedGroup.each(function() {

                searchContent = $(this).text().toLowerCase();

                if (searchContent.indexOf(searchedWord) != -1) {
                    $(this).css('display', 'block');
                } else {
                    $(this).css('display', 'none');
                }
                searchContent = '';


            })

              
        });

    }


function select($wrapper, $form) {
    // console.log($wrapper);
    $form.find("select").each(function () {
        var parent = $(this).parent();
        var firstLabel = $(this).find('option').first().html();

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + firstLabel + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        if ($(this).find('option').length > 5) {
            // odpalamy jezeli mamy wiecej niz 5 elementow
            parent.find("ul.dropdown-list").append('<li class="search" data-val="" ><input type="text" placeholder="Search..."></li>');
        }
        $('.dropdown-header span').addClass('placeholder');

        $(this).find('option').each(function () {
            if ($(this).val() != "") {
                parent.find("ul.dropdown-list").append('<li data-val="' + $(this).val() + '">' + $(this).text() + '</li>');
            }
        });

        if ($(this).find('option').length > 5) {
            selectSearch(); // odpalamy jezeli mamy wiecej niz 5 elementow
        }
    });

    $form.find('.dropdown_select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $form.find('.dropdown-list li').not('.search').click(function () {

        $('.dropdown-header span').removeClass('placeholder');

        var choose = $(this).text();
        var chooseVal = $(this).data('val');
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);

        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(chooseVal).find(' option[value="' + chooseVal + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
        smartSelect($(this));
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

function smartSelect(elem) {
  
    var dynamicDropdown = elem.parents('.hs-form-field').siblings('.hs-form-field');
    if (dynamicDropdown.parents('.hs-dependent-field').length > 0 && dynamicDropdown.length == 1 && dynamicDropdown.hasClass('rendered') == false) {
        //hideEmptyLabel();
        showAsterix();
        dynamicDropdown.addClass('rendered');
        var parent2 = dynamicDropdown.find("select").parent();
        var firstLabel = dynamicDropdown.find('option').first().html();
        //  console.log(firstLabel);
        parent2.addClass('dropdown_select');
        parent2.append('<div class="dropdown-header"><span>' + firstLabel + '</span></div>');
        parent2.append('<ul class="dropdown-list" style="display: none;"></ul>');

        if (dynamicDropdown.find('option').length > 5) {
            // odpalamy jezeli mamy wiecej niz 5 elementow
            parent2.find("ul.dropdown-list").append('<li class="search" data-val="" ><input type="text" placeholder="Search..."></li>');
        }

        $('.dropdown-header span').addClass('placeholder');

        dynamicDropdown.find('option').each(function () {
            if ($(this).val() != "") {
                parent2.find("ul.dropdown-list").append('<li data-val="' + $(this).val() + '">' + $(this).text() + '</li>');
            }
        });

        if (dynamicDropdown.find('option').length > 5) {
            selectSearch(); // odpalamy jezeli mamy wiecej niz 5 elementow
        }

        dynamicDropdown.find('.dropdown-header').click(function (event) {
            $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
            $(this).children('.arrow-white').toggle();
        });

        dynamicDropdown.find('.dropdown-list li').not('.search').click(function () {
            var choose = $(this).text();
            var chooseVal = $(this).data('val');
            $(this).parent().siblings('.dropdown-header').find('span').text(choose);
            $(this).parent().siblings('.dropdown-header').find('span').removeClass('placeholder');
            $(this).parent().siblings('select').find('option').removeAttr('selected');
            $(this).parent().siblings('select').val(chooseVal).find(' option[value="' + chooseVal + '"] ').attr('selected', 'selected').change();
            $(this).parent().find('li').removeClass('selected');
            $(this).addClass('selected');
            $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
            $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
        });
        dynamicDropdown.find('.dropdown_select .input').click(function (event) {
            event.stopPropagation();
        });
        // selectSearch();
    }
}





//waitForLoad(".form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", hideEmptyLabel)
waitForLoad(".form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", showAsterix);
waitForLoad(".form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", select);













