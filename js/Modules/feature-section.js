$(document).ready(function(){


	$('.feature-section .show').click(function() {

	  	$(this).parent().parent().parent().find('.more-content').toggleClass("active");

	  	if( $(this).hasClass('show-more') ){
	  		$(this).parent().find('.show').removeClass("disabled");
	  		$(this).addClass("disabled");
	  	} 

	  	if( $(this).hasClass('show-less') ){
	  		$(this).parent().find('.show').removeClass("disabled");
	  		$(this).addClass("disabled");
	  	} 

	});

	$('.feature-section .more-content .close').click(function() {

	  	$(this).parent().toggleClass("active");

	  	if( $(this).parent().parent().find('.show').hasClass('show-more') ){

	  		$(this).parent().parent().find('.show').removeClass("disabled");
	  		$(this).parent().parent().find('.show-more').addClass("disabled");

	  	} 

	  	if ( $(this).parent().parent().find('.show').hasClass('show-less') ){

	  		$(this).parent().parent().find('.show').addClass("disabled");
	  		$(this).parent().parent().find('.show-more').removeClass("disabled");
	  	} 

	});

})