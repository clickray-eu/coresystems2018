$(document).ready(function() {
    prepareEventRecordTabs();
});

function prepareEventRecordTabs() {

    $('.events-page__events .event').unwrap();

    var $events = [];

    $('.events-page__events .event').each(function() {
        $events.push(this);
    })
    $events.sort(function(a, b){
        return new Date($(a).data("date")).getTime()-new Date($(b).data("date")).getTime();
    });

    $('.events-page__events .event').parent().html($events);

}