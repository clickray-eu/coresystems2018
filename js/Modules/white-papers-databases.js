$(document).ready(function(){
		var firstFilter;  
		var secondFilter;

		// FIRST RUN
		$('.white-paper-db > div').each(function(i,e){
			// if(i < 4)
			$(this).addClass("active")
		})

		// FIRST SELECT 

		$('.filters #filter-1').change(function(){
			
			$('.white-paper-db > div').removeClass("active");
			$('.white-paper-db h4.no-results').remove();

			if( $('.filters #filter-2').val() != null){

				firstFilter = $(this).val();
				secondFilter = $('.filters #filter-2').val();

				$('.white-paper-db > div').each(function(){

					if( $(this).hasClass(firstFilter) && $(this).hasClass(secondFilter)){
						$(this).addClass("active")
					}

				})

			}else{
				firstFilter = $(this).val();

				$('.white-paper-db > div').each(function(){

					if($(this).hasClass(firstFilter)){
						$(this).addClass("active");
					}

				})
			}
			if( $('.white-paper-db > div.active').length  == 0){
				$('.white-paper-db').append('<h4 class="no-results">No Results...</h4>')
			}

		})


		// SECOND SELECT 

		$('.filters #filter-2').change(function(){

			$('.white-paper-db > div').removeClass("active");
			$('.white-paper-db h4.no-results').remove();

			if( $('.filters #filter-1').val() != null){

				firstFilter = $(this).val();
				secondFilter = $('.filters #filter-1').val();

				$('.white-paper-db > div').each(function(){

					if( $(this).hasClass(firstFilter) && $(this).hasClass(secondFilter)){
						$(this).addClass("active");
					}



				})

			}else{
				firstFilter = $(this).val();

				$('.white-paper-db > div').each(function(){

					if($(this).hasClass(firstFilter)){
						$(this).addClass("active");
					}

				})
			}
			if( $('.white-paper-db > div.active').length  == 0){
				$('.white-paper-db').append('<h4 class="no-results">No Results...</h4>')
			}
			

		});
	
});




// Select
function whitePapersSelect($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function (index) {
           
            if ( $(this).val() != "" ) {
                parent.find("ul.dropdown-list").append('<li data-filter="' + $(this).val() + '">' + $(this).text() + '</li>')
            }
        })
    })

    $form.find('.dropdown-header').click(function (event, element) {
 
        if (!($(this).hasClass('slide-down'))) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });


    $form.find('.dropdown-list li').click(function() {
        var choose = $(this).data("filter");
        var text = $(this).text();
    
        $(this).parent().siblings('.dropdown-header').find('span').text(text);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

waitForLoad(".industry-choose, .role-choose", "form", whitePapersSelect)






























