$(document).ready(function(){

	//showTopPanel();
	menuLayout();
	rebuildLangSwitcher();
	addRigthBarMobile();

	checkInnerWidth();
	ScrollHeaderMobile();

	progressBar();
})

$(window).resize(function(){
	checkInnerWidth();
})



function menuLayout(){

	//add id
	$('.core-main-menu').parent().parent().attr('id','menu-container');

	$('.btn-hamburger').click(function(){

		$(this).toggleClass("active");
		$('body').toggleClass("overflow");
		
		$('#menu-container').toggleClass("active");
		$('#menu-container li').removeClass("sub-active");
		$('#menu-container ul').removeClass("active");

	})


	$('.hs-menu-depth-1.hs-item-has-children').click(function(e){
		e.stopPropagation();
		if( $(this).hasClass('sub-active') ){
			
			$(this).find('> ul').toggleClass("active");
			$(this).toggleClass('sub-active');
			$('.hs-menu-depth-2').removeClass('sub-active');

		}else{

			$('.hs-item-has-children').find('> ul').removeClass("active");
			$(this).find('> ul').toggleClass("active");

			$('.hs-menu-depth-1').removeClass('sub-active');	
			$(this).toggleClass('sub-active');
			$('.hs-menu-depth-2').removeClass('sub-active');
		}
	})

	$('.hs-menu-depth-2.hs-item-has-children').click(function(e){
		e.stopPropagation();
		if( $(this).hasClass('sub-active') ){
				$(this).toggleClass('sub-active');	
		}else{
			$('.hs-menu-depth-2').removeClass('sub-active');	
			$(this).toggleClass('sub-active');	

		}
		

	})
}


function addRigthBarMobile(){


	var rightSide = $('.main-header .right-wrapper .get-started').clone(true); 

	$('.main-header .right-wrapper').parent().addClass('more-header')


	// $('#menu-container .hs-menu-wrapper>ul').append('<li class="more-menu"></li>');
	// $('#menu-container .more-menu').html(rightSide);

	$('header.main-header').append('<div class="mobile-menu-more clearfix"></li>');
	$('header.main-header .mobile-menu-more').html(rightSide);
	
}


function checkInnerWidth(){

		if ( $(window).innerWidth() < 1300) {
			$('header.main-header').addClass('mobile');
			$('.core-top-panel').addClass('mobile');
		} else  {
			$('header.main-header').removeClass('mobile');
			$('.core-top-panel').removeClass('mobile');
		}
}

function ScrollHeaderMobile() {

	var iScrollPos = 0;
  
  	if ($('.core-top-panel').length > 0) {
		var offsetTopPanel = $('.core-top-panel').outerHeight();

		$(window).resize(function() {
			offsetTopPanel = $('.core-top-panel').outerHeight();
		});
	}

  
	    $(window).scroll(function () {
	        
	        var iCurScrollPos = $(this).scrollTop();

	        if ($('.core-top-panel.mobile').length > 0 && $('header.mobile.main-header').length > 0) {

		        if (iCurScrollPos > iScrollPos && iScrollPos > 50 + offsetTopPanel) {
		        
		           $('header.mobile.main-header').addClass('scroll');
		           $('.core-top-panel.mobile').addClass('scroll');

		        } else {

		         
		           $('header.mobile.main-header').removeClass('scroll');
		           $('.core-top-panel.mobile').removeClass('scroll');

		        }
		    } else {

		        if (iCurScrollPos > iScrollPos && iScrollPos > 50) {
		        
		           $('header.mobile.main-header').addClass('scroll');

		        } else {

		         
		           $('header.mobile.main-header').removeClass('scroll');

		        }
		    }
	        iScrollPos = iCurScrollPos;

	    });
}

function rebuildLangSwitcher() {
	if ($('.main-header .lang_switcher_class').length > 0) {
		// rebuild
		var langSwitcher = $('.main-header .lang_switcher_class .globe_class');

		langSwitcher.find('ul.lang_list_class li').each(function() {
			var lang = $(this).find('a.lang_switcher_link');
			var langLabel = lang.data('language');
			lang.text(langLabel);
		});

		var mobileLangSwitcher = langSwitcher.find('.lang_list_class').clone();
		$('.core-main-menu ul').first().append('<li class="mobile-lang_switcher"><ul>' + mobileLangSwitcher.html() + '</ul></li>');

		var activeLang = langSwitcher.find('ul.lang_list_class > li:first-of-type a');
		var activeLangLabel = activeLang.data('language');
		activeLang.parents('li').remove();
		langSwitcher.prepend('<button class="active-lang">' + activeLangLabel + '</div>');
	
		// toggle show/hide lang list
		$('.globe_class .active-lang').on('click', function() {
			$(this).siblings('.lang_list_class').toggleClass('lang_list_class--show');
		}) 
	}

}

function progressBar() {
	var progressElem = $('.scroll-indicator');
	var relatedElem = $('.blog-single');
	if (progressElem.length > 0) {
		var winHeight = $(window).height();
		var docHeight = $(relatedElem).height() + $(relatedElem).position().top;
		var max = docHeight - winHeight;
		var value = $(window).scrollTop();

		$(progressElem).attr('max', max);
		$(progressElem).attr('value', value);

		$(window).on('resize', function() {
			winHeight = $(window).height();
			docHeight = $(relatedElem).height() + $(relatedElem).position().top;

			max = docHeight - winHeight;
			progressElem.attr('max', max);

		  value =  $(window).scrollTop();
		  progressElem.attr('value', value);
		});

		$(document).on('scroll', function() {
			value = $(window).scrollTop();
			progressElem.attr('value', value);
		});
	}
}

function showTopPanel() {
	if ($('.core-top-panel').length > 0) {
		var acctualPosition = $(window).scrollTop();
		var offsetTop = $('.core-top-panel').outerHeight();

		$(window).resize(function() {
			acctualPosition = $(window).scrollTop();
			offsetTop = $('.core-top-panel').outerHeight();
			$('.body-container-wrapper').css('margin-top', offsetTop + 'px');
			if (acctualPosition > offsetTop) {
				$('.main-header').css({'position' : 'fixed', 'margin-top': '0px'});
			} else {
				$('.main-header').css({'position' : 'absolute', 'margin-top': offsetTop + 'px'});
			}
		});

		$(window).scroll(function () {
			acctualPosition = $(window).scrollTop();
			if (acctualPosition > offsetTop) {
				$('.main-header').css({'position' : 'fixed', 'margin-top': '0px'});
			} else {
				$('.main-header').css({'position' : 'absolute', 'margin-top': offsetTop + 'px'});
			}

		});

		$(window).scroll();
		$(window).resize();
	}
}
