$('button[rel="core-blog-subscribe"]').on('click', function() {
	$('#core-blog-subscribe').fadeToggle();
});

if ($('.core18-blog').hasClass('hs-blog-listing')) {
	var firstPost = $('.blog-feed__box').first().offset();
	var lastPost = $('#end-indicator').offset();
	var containerHeight = lastPost.top - firstPost.top;
	$('.blog-feed .container').css('height', containerHeight);

	$(window).resize(function() {
		if ($(window).width() > 991) {
			firstPost = $('.blog-feed__box').first().offset();
            lastPost = $('#end-indicator').offset();
            containerHeight = lastPost.top - firstPost.top;
            $('.blog-feed .container').css('height', containerHeight);
        }
    });
}