$(document).ready(function(){
	if ($('body').hasClass('case-studies') || $('.case-studies').hasClass('content-module')) {
		if ( $('.case-studies-single .social-share ul li').length > 0 ) {
			var positionStart = $('.case-studies-single').position().top;
			var positionEnd = $('.case-studies-single').position().top + $('.case-studies-single').innerHeight() - $('.social-share ul').innerHeight() - 100;
			var acctualPosition, fixedPosition;

			$(window).resize(function () {
	          positionStart = $('.case-studies-single').position().top;
	          positionEnd = $('.case-studies-single').position().top + $('.case-studies-single').innerHeight() - $('.social-share ul').innerHeight() - 100;
	        });

	       	$(window).scroll(function () {
				acctualPosition = $(window).scrollTop() + 100;

				if(acctualPosition > positionStart && acctualPosition <= positionEnd){
					$('.social-share ul').css({position: "fixed", top: "110px" })
				}
				else if(acctualPosition > positionEnd){
					$('.social-share ul').css({position: "absolute", top: "auto",bottom: "50px" })
				}
				else{
					$('.social-share ul').css({position: "absolute", top: "10px" })
				}
			});
		}

		$(window).scroll();
	}


	if ($('body').hasClass('core18-blog')) {
		if ( $('.case-studies-single .social-share ul li').length > 0 ) {
			var socialsHeight = $('.social-share ul').innerHeight();
			var positionStart = $('.case-studies-single').position().top;
			var positionEnd = $('.case-studies-single').position().top + $('.case-studies-single').innerHeight() - socialsHeight - 100;
			var acctualPosition, fixedPosition;

			$(window).resize(function () {
	          positionStart = $('.case-studies-single').position().top;
	          positionEnd = $('.case-studies-single').position().top + $('.case-studies-single').innerHeight() - socialsHeight - 100;
	          acctualPosition = $(window).scrollTop() + $(window).height()/2 - socialsHeight/2;
			  fixedPosition = $(window).height()/2 - socialsHeight/2;
	        });

	       	$(window).scroll(function () {
				acctualPosition = $(window).scrollTop() + $(window).height()/2 - socialsHeight/2;
				fixedPosition = $(window).height()/2 - socialsHeight/2;

				if(acctualPosition > positionStart && acctualPosition <= positionEnd){
					$('.social-share ul').css({position: "fixed", top: fixedPosition + "px" })
				}
				else if(acctualPosition > positionEnd){
					$('.social-share ul').css({position: "absolute", top: "auto",bottom: "100px" })
				}
				else{
					$('.social-share ul').css({position: "absolute", top: "0px" })
				}
			});
		}

		$(window).scroll();
	}
})
