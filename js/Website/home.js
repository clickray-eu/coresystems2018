$(window).load(function(){


    $('.home .slider-wrapper').css('height', 'auto');

})


$(document).ready(function(){

    // ACTIONS


    var itemCount =  $('.home .slider-wrapper .hero-slide').length


    if (itemCount > 0) {
        $('.home .slider-wrapper').append('<div class="main-slider"></div> <div class="nav-slider"></div>')


         if(itemCount <= 3)$('.home .slider-wrapper .nav-slider').addClass('below-min');

        

        $('.home .slider-wrapper .hero-slide').each(function(){
          $(this).appendTo('.slider-wrapper .main-slider');
        })

            // HOME SLIDER
            $('.home .main-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true,
                autoplay: true,
                asNavFor: '.nav-slider',
                responsive: [
                    {
                        breakpoint: 1230,
                        settings: {
                            arrows: false
                        }
                    }
                ]
            });

        if ($('.home .slider-wrapper .pagination-content')) {
            $('.home .slider-wrapper .pagination-content').each(function(){
                $(this).appendTo('.slider-wrapper .nav-slider');
            })
        
            $('.home .nav-slider').slick({
                slidesToShow: 3,
                centerMode: true,
                centerPadding: 0,
                arrows: false,
                slidesToScroll: 1,
                asNavFor: '.main-slider',
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 1120,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 680,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        }
    }

    // PERSON SLIDER

    if ($('.home .persons-wrapper .single-person').length > 0) {
        $('.home .persons-wrapper > span, .home .persons-wrapper.global').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1120,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 680,
                    settings: {
                        centerMode: true,
                        centerPadding: '50px',
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true
                    }
                }
            ]
        });
    }
    // INDUSTRIES SLIDER
if ($('.home .slider-industries-wrapper .single-industries').length > 0) {
    $('.home .slider-industries-wrapper > span').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 1120,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 680,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    arrows: true
                }
            }
        ]
    });
}
    // VIDEO SLIDER 
    videoSliderInit();

    function videoSliderInit(){

        if ($('.home .video-slider-wrapper .video-slide').length > 0) {
            $('.video-slider-wrapper').parent().parent().append('<div class="video-desc-slider"></div>')

            $('.video-slide .description').each(function(){
              $(this).appendTo('.video-desc-slider');
            })

            $('.home .video-slider-wrapper > span').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                // autoplay: true,
                asNavFor: '.video-desc-slider',
                arrows: true,
                dots: true, 
                centerMode: true,
                centerPadding: 0,
                responsive: [
                    {
                        breakpoint: 1250,
                        settings: {
                            slidesToShow: 2,
                            centerMode: false,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToShow: 1,
                            centerMode: false,
                            autoplay: true,
                            arrows: false,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

            $('.home .video-desc-slider').slick({
                slidesToShow: 1,
                centerMode: true,
                centerPadding: 0,
                arrows: false,
                dots: false,
                slidesToScroll: 1,
                draggable: false,
                 asNavFor: '.video-slider-wrapper > span',
                // focusOnSelect: true
            });
        }

    }
    


    // LOGOS SLIDER
    if ($('.home .logo-panel-db .logo-item').length > 0) {
        $('.home .logo-panel-db').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 680,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: false,
                        arrows: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        arrows: true
                    }
                }
            ]
        });
    }
    // 
})



