$(document).ready(function () {

    detectBrowser(); 
    detectOS();
    // alert('hej!');
    
   
})

function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}

function detectBrowser() {

    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    if (iOS) $('body').addClass('ios');

    if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
        $('body').addClass('ms-browser');
    }

    if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
      $('body').addClass('ff');
    }

}


function detectOS(){

    var OSName = "Unknown";
    if (window.navigator.userAgent.indexOf("Windows NT 10.0")!= -1) OSName="windows";
    if (window.navigator.userAgent.indexOf("Windows NT 6.2") != -1) OSName="windows";
    if (window.navigator.userAgent.indexOf("Windows NT 6.1") != -1) OSName="windows";
    if (window.navigator.userAgent.indexOf("Windows NT 6.0") != -1) OSName="windows";
    if (window.navigator.userAgent.indexOf("Windows NT 5.1") != -1) OSName="windows";
    if (window.navigator.userAgent.indexOf("Windows NT 5.0") != -1) OSName="windows";
    if (window.navigator.userAgent.indexOf("Mac")            != -1) OSName="mac";
    if (window.navigator.userAgent.indexOf("X11")            != -1) OSName="UNIX";
    if (window.navigator.userAgent.indexOf("Linux")          != -1) OSName="Linux";

    $('body').addClass(OSName);

}