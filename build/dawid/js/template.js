'use strict';

// file: footer.js
$(document).ready(function () {

    $('footer.footer .mobile-active').parent().parent().addClass('mobile-active-wrapper');
});
// end file: footer.js

// file: form.js
function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).css({ "display": "none" });
        }
    });
}

// Select


function selectSearch() {

    var searchContent = '';

    $('.dropdown-list .search').keyup(function (e) {

        var searchedWord = e.target.value.toLowerCase();
        var searchedGroup = $(this).parent().find('li').not('.search');

        searchedGroup.each(function () {

            searchContent = $(this).text().toLowerCase();

            if (searchContent.indexOf(searchedWord) != -1) {
                $(this).css('display', 'block');
            } else {
                $(this).css('display', 'none');
            }
            searchContent = '';
        });
    });
}

function select($wrapper, $form) {
    // console.log($wrapper);
    $form.find("select").each(function () {
        var parent = $(this).parent();
        var firstLabel = $(this).find('option').first().html();

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + firstLabel + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        if ($(this).find('option').length > 5) {
            // odpalamy jezeli mamy wiecej niz 5 elementow
            parent.find("ul.dropdown-list").append('<li class="search" value="" ><input type="text" placeholder="Search..."></li>');
        }
        $('.dropdown-header span').addClass('placeholder');

        $(this).find('option').each(function () {
            if ($(this).val() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).text() + '</li>');
            }
        });

        if ($(this).find('option').length > 5) {
            selectSearch(); // odpalamy jezeli mamy wiecej niz 5 elementow
        }
    });

    $form.find('.dropdown_select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $form.find('.dropdown-list li').not('.search').click(function () {

        $('.dropdown-header span').removeClass('placeholder');

        var choose = $(this).text();
        var chooseVal = $(this).attr('value');
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);

        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(chooseVal).find(' option[value="' + chooseVal + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
        smartSelect($(this));
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

function smartSelect(elem) {

    var dynamicDropdown = elem.parents('.hs-form-field').siblings('.hs-form-field');
    if (dynamicDropdown.parents('.hs-dependent-field').length > 0 && dynamicDropdown.length == 1 && dynamicDropdown.hasClass('rendered') == false) {
        hideEmptyLabel();
        dynamicDropdown.addClass('rendered');
        var parent2 = dynamicDropdown.find("select").parent();
        var firstLabel = dynamicDropdown.find('option').first().html();
        //  console.log(firstLabel);
        parent2.addClass('dropdown_select');
        parent2.append('<div class="dropdown-header"><span>' + firstLabel + '</span></div>');
        parent2.append('<ul class="dropdown-list" style="display: none;"></ul>');

        if (dynamicDropdown.find('option').length > 5) {
            // odpalamy jezeli mamy wiecej niz 5 elementow
            parent2.find("ul.dropdown-list").append('<li class="search" value="" ><input type="text" placeholder="Search..."></li>');
        }

        $('.dropdown-header span').addClass('placeholder');

        dynamicDropdown.find('option').each(function () {
            if ($(this).val() != "") {
                parent2.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).text() + '</li>');
            }
        });

        if (dynamicDropdown.find('option').length > 5) {
            selectSearch(); // odpalamy jezeli mamy wiecej niz 5 elementow
        }

        dynamicDropdown.find('.dropdown-header').click(function (event) {
            $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
            $(this).children('.arrow-white').toggle();
        });

        dynamicDropdown.find('.dropdown-list li').not('.search').click(function () {
            var choose = $(this).text();
            var chooseVal = $(this).attr('value');
            $(this).parent().siblings('.dropdown-header').find('span').text(choose);
            $(this).parent().siblings('.dropdown-header').find('span').removeClass('placeholder');
            $(this).parent().siblings('select').find('option').removeAttr('selected');
            $(this).parent().siblings('select').val(chooseVal).find(' option[value="' + chooseVal + '"] ').attr('selected', 'selected').change();
            $(this).parent().find('li').removeClass('selected');
            $(this).addClass('selected');
            $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
            $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
        });
        dynamicDropdown.find('.dropdown_select .input').click(function (event) {
            event.stopPropagation();
        });
        // selectSearch();
    }
}

waitForLoad(".form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", hideEmptyLabel);
waitForLoad(".form-wrapper, .widget-type-blog_comments, .hs_cos_wrapper_type_form", "form", select);

// end file: form.js

// file: global.js
$(document).ready(function () {

    detectBrowser();
    detectOS();
    // alert('hej!');

});

function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}

function detectBrowser() {

    var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    if (iOS) $('body').addClass('ios');

    if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /MSIE 10/i.test(navigator.userAgent) || /Edge\/12./i.test(navigator.userAgent) || window.navigator.userAgent.indexOf("Edge") > -1) {
        $('body').addClass('ms-browser');
    }

    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        $('body').addClass('ff');
    }
}

function detectOS() {

    var OSName = "Unknown";
    if (window.navigator.userAgent.indexOf("Windows NT 10.0") != -1) OSName = "windows";
    if (window.navigator.userAgent.indexOf("Windows NT 6.2") != -1) OSName = "windows";
    if (window.navigator.userAgent.indexOf("Windows NT 6.1") != -1) OSName = "windows";
    if (window.navigator.userAgent.indexOf("Windows NT 6.0") != -1) OSName = "windows";
    if (window.navigator.userAgent.indexOf("Windows NT 5.1") != -1) OSName = "windows";
    if (window.navigator.userAgent.indexOf("Windows NT 5.0") != -1) OSName = "windows";
    if (window.navigator.userAgent.indexOf("Mac") != -1) OSName = "mac";
    if (window.navigator.userAgent.indexOf("X11") != -1) OSName = "UNIX";
    if (window.navigator.userAgent.indexOf("Linux") != -1) OSName = "Linux";

    $('body').addClass(OSName);
}
// end file: global.js

// file: header.js
$(document).ready(function () {

    menuLayout();
    rebuildLangSwitcher();
    addRigthBarMobile();

    checkInnerWidth();
    ScrollHeaderMobile();

    progressBar();
});

$(window).resize(function () {
    checkInnerWidth();
});

function menuLayout() {

    //add id
    $('.core-main-menu').parent().parent().attr('id', 'menu-container');

    $('.btn-hamburger').click(function () {

        $(this).toggleClass("active");
        $('body').toggleClass("overflow");

        $('#menu-container').toggleClass("active");
        $('#menu-container li').removeClass("sub-active");
        $('#menu-container ul').removeClass("active");
    });

    $('.hs-menu-depth-1.hs-item-has-children').click(function (e) {
        e.stopPropagation();
        if ($(this).hasClass('sub-active')) {

            $(this).find('> ul').toggleClass("active");
            $(this).toggleClass('sub-active');
            $('.hs-menu-depth-2').removeClass('sub-active');
        } else {

            $('.hs-item-has-children').find('> ul').removeClass("active");
            $(this).find('> ul').toggleClass("active");

            $('.hs-menu-depth-1').removeClass('sub-active');
            $(this).toggleClass('sub-active');
            $('.hs-menu-depth-2').removeClass('sub-active');
        }
    });

    $('.hs-menu-depth-2.hs-item-has-children').click(function (e) {
        e.stopPropagation();
        if ($(this).hasClass('sub-active')) {
            $(this).toggleClass('sub-active');
        } else {
            $('.hs-menu-depth-2').removeClass('sub-active');
            $(this).toggleClass('sub-active');
        }
    });
}

function addRigthBarMobile() {

    var rightSide = $('.main-header .right-wrapper .get-started').clone(true);

    $('.main-header .right-wrapper').parent().addClass('more-header');

    // $('#menu-container .hs-menu-wrapper>ul').append('<li class="more-menu"></li>');
    // $('#menu-container .more-menu').html(rightSide);

    $('header.main-header').append('<div class="mobile-menu-more clearfix"></li>');
    $('header.main-header .mobile-menu-more').html(rightSide);
}

function checkInnerWidth() {

    if ($(window).innerWidth() < 1300) $('header.main-header').addClass('mobile');else $('header.main-header').removeClass('mobile');
}

function ScrollHeaderMobile() {

    var iScrollPos = 0;

    $(window).scroll(function () {

        var iCurScrollPos = $(this).scrollTop();
        if (iCurScrollPos > iScrollPos && iScrollPos > 50) {

            $('header.mobile.main-header').addClass('scroll');
        } else {

            $('header.mobile.main-header').removeClass('scroll');
        }
        iScrollPos = iCurScrollPos;
    });
}

function rebuildLangSwitcher() {
    if ($('.main-header .lang_switcher_class').length > 0) {
        // rebuild
        var langSwitcher = $('.main-header .lang_switcher_class .globe_class');

        langSwitcher.find('ul.lang_list_class li').each(function () {
            var lang = $(this).find('a.lang_switcher_link');
            var langLabel = lang.data('language');
            lang.text(langLabel);
        });

        var mobileLangSwitcher = langSwitcher.find('.lang_list_class').clone();
        $('.core-main-menu ul').first().append('<li class="mobile-lang_switcher"><ul>' + mobileLangSwitcher.html() + '</ul></li>');

        var activeLang = langSwitcher.find('ul.lang_list_class > li:first-of-type a');
        var activeLangLabel = activeLang.data('language');
        activeLang.parents('li').remove();
        langSwitcher.prepend('<button class="active-lang">' + activeLangLabel + '</div>');

        // toggle show/hide lang list
        $('.globe_class .active-lang').on('click', function () {
            $(this).siblings('.lang_list_class').toggleClass('lang_list_class--show');
        });
    }
}

function progressBar() {
    var progressElem = $('.scroll-indicator');
    var relatedElem = $('.blog-single');
    if (progressElem.length > 0) {
        var winHeight = $(window).height();
        var docHeight = $(relatedElem).height() + $(relatedElem).position().top;
        var max = docHeight - winHeight;
        var value = $(window).scrollTop();

        $(progressElem).attr('max', max);
        $(progressElem).attr('value', value);

        $(window).on('resize', function () {
            winHeight = $(window).height();
            docHeight = $(relatedElem).height() + $(relatedElem).position().top;

            max = docHeight - winHeight;
            progressElem.attr('max', max);

            value = $(window).scrollTop();
            progressElem.attr('value', value);
        });

        $(document).on('scroll', function () {
            value = $(window).scrollTop();
            progressElem.attr('value', value);
        });
    }
}

// end file: header.js

// file: systems-pages.js
$(document).ready(function () {

    // Rebuild Layout Update Data
    if ($('body').hasClass('subscription-preferences')) {
        $('.widget-type-email_subscriptions .page-header').appendTo('.top-section');
    }

    if ($('body').hasClass('systems')) {
        var imgURL = $('.hero-banner-system-page img').attr('src');
        $('.hero-banner-system-page').css('background-image', 'url(' + imgURL + ')');
    }

    if ($('body').hasClass('subscription-confirmation')) {

        $('#content').appendTo('.page-header').insertAfter('h2');
    }
});
// end file: systems-pages.js

// file: Landing/landing.js
$(document).ready(function () {

    // LOGOS SLIDER

    $('.landing .logo-panel-db').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        arrows: true,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 680,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                dots: false,
                arrows: true
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true
            }
        }]
    });
});
// end file: Landing/landing.js

// file: Blog/case-studies.js
$(document).ready(function () {
    if ($('body').hasClass('case-studies') || $('.case-studies').hasClass('content-module')) {
        if ($('.case-studies-single .social-share ul li').length > 0) {
            var positionStart = $('.case-studies-single').position().top;
            var positionEnd = $('.case-studies-single').position().top + $('.case-studies-single').innerHeight() - $('.social-share ul').innerHeight() - 100;
            var acctualPosition, fixedPosition;

            $(window).resize(function () {
                positionStart = $('.case-studies-single').position().top;
                positionEnd = $('.case-studies-single').position().top + $('.case-studies-single').innerHeight() - $('.social-share ul').innerHeight() - 100;
            });

            $(window).scroll(function () {
                acctualPosition = $(window).scrollTop() + 100;

                if (acctualPosition > positionStart && acctualPosition <= positionEnd) {
                    $('.social-share ul').css({ position: "fixed", top: "110px" });
                } else if (acctualPosition > positionEnd) {
                    $('.social-share ul').css({ position: "absolute", top: "auto", bottom: "50px" });
                } else {
                    $('.social-share ul').css({ position: "absolute", top: "10px" });
                }
            });
        }

        $(window).scroll();
    }

    if ($('body').hasClass('core18-blog')) {
        if ($('.case-studies-single .social-share ul li').length > 0) {
            var socialsHeight = $('.social-share ul').innerHeight();
            var positionStart = $('.case-studies-single').position().top;
            var positionEnd = $('.case-studies-single').position().top + $('.case-studies-single').innerHeight() - socialsHeight - 100;
            var acctualPosition, fixedPosition;

            $(window).resize(function () {
                positionStart = $('.case-studies-single').position().top;
                positionEnd = $('.case-studies-single').position().top + $('.case-studies-single').innerHeight() - socialsHeight - 100;
                acctualPosition = $(window).scrollTop() + $(window).height() / 2 - socialsHeight / 2;
                fixedPosition = $(window).height() / 2 - socialsHeight / 2;
            });

            $(window).scroll(function () {
                acctualPosition = $(window).scrollTop() + $(window).height() / 2 - socialsHeight / 2;
                fixedPosition = $(window).height() / 2 - socialsHeight / 2;

                if (acctualPosition > positionStart && acctualPosition <= positionEnd) {
                    $('.social-share ul').css({ position: "fixed", top: fixedPosition + "px" });
                } else if (acctualPosition > positionEnd) {
                    $('.social-share ul').css({ position: "absolute", top: "auto", bottom: "100px" });
                } else {
                    $('.social-share ul').css({ position: "absolute", top: "0px" });
                }
            });
        }

        $(window).scroll();
    }
});

// end file: Blog/case-studies.js

// file: Blog/core18-blog.js
$('button[rel="core-blog-subscribe"]').on('click', function () {
    $('#core-blog-subscribe').fadeToggle();
});

if ($('.core18-blog').hasClass('hs-blog-listing')) {
    var firstPost = $('.blog-feed__box').first().offset();
    var lastPost = $('#end-indicator').offset();
    var containerHeight = lastPost.top - firstPost.top;
    $('.blog-feed .container').css('height', containerHeight);

    $(window).resize(function () {
        if ($(window).width() > 991) {
            firstPost = $('.blog-feed__box').first().offset();
            lastPost = $('#end-indicator').offset();
            containerHeight = lastPost.top - firstPost.top;
            $('.blog-feed .container').css('height', containerHeight);
        }
    });
}
// end file: Blog/core18-blog.js

// file: Modules/event.js
$(document).ready(function () {
    prepareEventRecordTabs();
});

function prepareEventRecordTabs() {

    $('.events-page__events .event').unwrap();

    var $events = [];

    $('.events-page__events .event').each(function () {
        $events.push(this);
    });
    $events.sort(function (a, b) {
        return new Date($(a).data("date")).getTime() - new Date($(b).data("date")).getTime();
    });

    $('.events-page__events .event').parent().html($events);
}
// end file: Modules/event.js

// file: Modules/feature-section.js
$(document).ready(function () {

    $('.feature-section .show').click(function () {

        $(this).parent().parent().parent().find('.more-content').toggleClass("active");

        if ($(this).hasClass('show-more')) {
            $(this).parent().find('.show').removeClass("disabled");
            $(this).addClass("disabled");
        }

        if ($(this).hasClass('show-less')) {
            $(this).parent().find('.show').removeClass("disabled");
            $(this).addClass("disabled");
        }
    });

    $('.feature-section .more-content .close').click(function () {

        $(this).parent().toggleClass("active");

        if ($(this).parent().parent().find('.show').hasClass('show-more')) {

            $(this).parent().parent().find('.show').removeClass("disabled");
            $(this).parent().parent().find('.show-more').addClass("disabled");
        }

        if ($(this).parent().parent().find('.show').hasClass('show-less')) {

            $(this).parent().parent().find('.show').addClass("disabled");
            $(this).parent().parent().find('.show-more').removeClass("disabled");
        }
    });
});
// end file: Modules/feature-section.js

// file: Modules/kpi-box.js


// COUNTER


$(window).scroll(function () {

    checkCounterPosition();
});

$(document).ready(function () {

    checkCounterPosition();
});

// VARIABELS 


var checkcounter = 0;

function countTo() {

    $('.kpi-box .count-active').each(function () {

        var $this = $(this);
        var countTo = $this.attr('data-count');

        $this = $(this).find('span');

        $({ countNum: $this.text() }).animate({
            countNum: countTo
        }, {

            duration: 1000,
            easing: 'swing',
            step: function step() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function complete() {
                $this.text(this.countNum);
            }

        });
    });
    checkcounter = 1;
}

function checkCounterPosition() {
    var counterposition;
    if ($(".kpi-boxes .kpi-box .count-active").length != 0) {
        counterposition = $(".kpi-boxes .kpi-box").position().top + 100;
        if ($(window).scrollTop() + $(window).height() > counterposition && checkcounter == 0) {
            countTo();
        }
    } else {
        return 0;
    }
}
// end file: Modules/kpi-box.js

// file: Modules/white-papers-databases.js
$(document).ready(function () {

    var firstFilter;
    var secondFilter;

    // FIRST RUN
    $('.white-paper-db > div').each(function (i, e) {
        // if(i < 4)
        $(this).addClass("active");
    });

    // FIRST SELECT 

    $('.filters #filter-1').change(function () {

        $('.white-paper-db > div').removeClass("active");
        $('.white-paper-db h4.no-results').remove();

        if ($('.filters #filter-2').val() != null) {

            firstFilter = $(this).val();
            secondFilter = $('.filters #filter-2').val();

            $('.white-paper-db > div').each(function () {

                if ($(this).hasClass(firstFilter) && $(this).hasClass(secondFilter)) {
                    $(this).addClass("active");
                }
            });
        } else {
            firstFilter = $(this).val();

            $('.white-paper-db > div').each(function () {

                if ($(this).hasClass(firstFilter)) {
                    $(this).addClass("active");
                }
            });
        }
        if ($('.white-paper-db > div.active').length == 0) {
            $('.white-paper-db').append('<h4 class="no-results">No Results...</h4>');
        }
    });

    // SECOND SELECT 

    $('.filters #filter-2').change(function () {

        $('.white-paper-db > div').removeClass("active");
        $('.white-paper-db h4.no-results').remove();

        if ($('.filters #filter-1').val() != null) {

            firstFilter = $(this).val();
            secondFilter = $('.filters #filter-1').val();

            $('.white-paper-db > div').each(function () {

                if ($(this).hasClass(firstFilter) && $(this).hasClass(secondFilter)) {
                    $(this).addClass("active");
                }
            });
        } else {
            firstFilter = $(this).val();

            $('.white-paper-db > div').each(function () {

                if ($(this).hasClass(firstFilter)) {
                    $(this).addClass("active");
                }
            });
        }
        if ($('.white-paper-db > div.active').length == 0) {
            $('.white-paper-db').append('<h4 class="no-results">No Results...</h4>');
        }
    });
});

// Select
function whitePapersSelect($wrapper, $form) {
    if ($form.hasClass('new-select-custom')) {
        return;
    }
    $form.addClass('new-select-custom');
    $form.find("select").each(function () {
        var parent = $(this).parent(),
            options = $(this).find('option'),
            placeholder = options.first().text() ? options.first().text() : '-Select-';

        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>' + placeholder + '</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        options.not(":disabled").each(function (index) {

            if ($(this).text() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).text().toLowerCase().split(" ").join("-") + '">' + $(this).text() + '</li>');
            }
        });
    });

    $form.find('.dropdown-header').click(function (event, element) {

        if (!$(this).hasClass('slide-down')) {
            $(this).addClass('slide-down');
            $(this).siblings('.dropdown-list').slideDown();
        } else {
            $(this).removeClass('slide-down');
            $(this).siblings('.dropdown-list').slideUp();
        }
        $(this).children('.arrow-white').toggle();
    });

    $form.find('.dropdown-list li').click(function () {
        var choose = $(this).attr("value");
        var text = $(this).text();

        $(this).parent().siblings('.dropdown-header').find('span').text(text);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideUp();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $form.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

waitForLoad(".industry-choose, .role-choose", "form", whitePapersSelect);

// end file: Modules/white-papers-databases.js

// file: Website/home.js
$(window).load(function () {

    $('.home .slider-wrapper').css('height', 'auto');
});

$(document).ready(function () {

    // ACTIONS


    var itemCount = $('.home .slider-wrapper .hero-slide').length;

    $('.home .slider-wrapper').append('<div class="main-slider"></div> <div class="nav-slider"></div>');

    if (itemCount <= 3) $('.home .slider-wrapper .nav-slider').addClass('below-min');

    $('.home .slider-wrapper .hero-slide').each(function () {
        $(this).appendTo('.slider-wrapper .main-slider');
    });

    // HOME SLIDER
    $('.home .main-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        autoplay: true,
        asNavFor: '.nav-slider',
        responsive: [{
            breakpoint: 1230,
            settings: {
                arrows: false
            }
        }]
    });

    if ($('.home .slider-wrapper .pagination-content')) {
        $('.home .slider-wrapper .pagination-content').each(function () {
            $(this).appendTo('.slider-wrapper .nav-slider');
        });

        $('.home .nav-slider').slick({
            slidesToShow: 3,
            centerMode: true,
            centerPadding: 0,
            arrows: false,
            slidesToScroll: 1,
            asNavFor: '.main-slider',
            focusOnSelect: true,
            responsive: [{
                breakpoint: 1120,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 680,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    }

    // PERSON SLIDER

    $('.home .persons-wrapper > span, .home .persons-wrapper.global').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        responsive: [{
            breakpoint: 1120,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 680,
            settings: {
                centerMode: true,
                centerPadding: '50px',
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true
            }
        }]
    });

    // INDUSTRIES SLIDER

    $('.home .slider-industries-wrapper > span').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        arrows: false,
        responsive: [{
            breakpoint: 1120,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 680,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: true
            }
        }]
    });

    // VIDEO SLIDER 
    videoSliderInit();

    function videoSliderInit() {

        $('.video-slider-wrapper').parent().parent().append('<div class="video-desc-slider"></div>');

        $('.video-slide .description').each(function () {
            $(this).appendTo('.video-desc-slider');
        });

        $('.home .video-slider-wrapper > span').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            // autoplay: true,
            asNavFor: '.video-desc-slider',
            arrows: true,
            dots: true,
            centerMode: true,
            centerPadding: 0,
            responsive: [{
                breakpoint: 1250,
                settings: {
                    slidesToShow: 2,
                    centerMode: false,
                    slidesToScroll: 1
                }
            }, {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    centerMode: false,
                    autoplay: true,
                    arrows: false,
                    slidesToScroll: 1
                }
            }]
        });

        $('.home .video-desc-slider').slick({
            slidesToShow: 1,
            centerMode: true,
            centerPadding: 0,
            arrows: false,
            dots: false,
            slidesToScroll: 1,
            draggable: false,
            asNavFor: '.video-slider-wrapper > span'
            // focusOnSelect: true
        });
    }

    // LOGOS SLIDER

    $('.home .logo-panel-db').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        arrows: true,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 680,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                dots: false,
                arrows: true
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                arrows: true
            }
        }]
    });

    // 
});

// end file: Website/home.js

// file: Website/terms-of-service.js
$(document).ready(function () {

    if ($('body').hasClass('terms-of-service')) {

        var positionStart = $('.terms-of-service .quick-menu-wrapper').position().top;
        var positionEnd = $('.content').position().top + $('.content').innerHeight() - $('.quick-menu-wrapper > div').innerHeight(); // -100
        var menuWidth = $('.quick-menu-wrapper').innerWidth() - 7;
        var acctualPosition;

        $(window).resize(function () {
            positionStart = $('.terms-of-service .quick-menu-wrapper').position().top;
            positionEnd = $('.content').position().top + $('.content').innerHeight() - $('.quick-menu-wrapper > div').innerHeight() - 100;
            menuWidth = $('.quick-menu-wrapper').innerWidth() - 7;
            $('.quick-menu-wrapper > div').css({ width: menuWidth + "px" });;
        });

        $(window).scroll(function () {

            acctualPosition = $(window).scrollTop() + 150;

            if (acctualPosition > positionStart && acctualPosition <= positionEnd) {
                $('.quick-menu-wrapper > div').css({ position: "fixed", bottom: "auto", top: "150px", width: menuWidth + "px" });
                // console.log('scroll');
            } else if (acctualPosition > positionEnd) {
                $('.quick-menu-wrapper > div').css({ position: "absolute", top: "auto", bottom: "100px", width: menuWidth + "px" });
                // console.log('bottom');
            } else {
                $('.quick-menu-wrapper > div').css({ position: "absolute", bottom: "auto", top: "0px", width: menuWidth + "px" });
                // console.log('top');
            }
        });
    }
});

// end file: Website/terms-of-service.js
//# sourceMappingURL=template.js.map
